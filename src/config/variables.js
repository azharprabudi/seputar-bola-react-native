import axios from "axios";
export const _axios = axios.create({
  baseURL: "https://api.football-data.org/v1/",
  timeout: 2000,
  headers: {
    "X-Auth-Token": "3ec58f1924ed444fa4ad10b56a84ef9f"
  }
});
